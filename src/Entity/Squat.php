<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SquatRepository")
 */
class Squat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $humanNb;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $location;

    /**
     * @ORM\Column(type="boolean")
     */
    private $food;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Kitten", inversedBy="squats")
     */
    private $cats;

    public function __construct()
    {
        $this->cats = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getHumanNb(): ?int
    {
        return $this->humanNb;
    }

    public function setHumanNb(int $humanNb): self
    {
        $this->humanNb = $humanNb;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getFood(): ?bool
    {
        return $this->food;
    }

    public function setFood(bool $food): self
    {
        $this->food = $food;

        return $this;
    }

    /**
     * @return Collection|Kitten[]
     */
    public function getCats(): Collection
    {
        return $this->cats;
    }

    public function addCat(Kitten $cat): self
    {
        if (!$this->cats->contains($cat)) {
            $this->cats[] = $cat;
        }

        return $this;
    }

    public function removeCat(Kitten $cat): self
    {
        if ($this->cats->contains($cat)) {
            $this->cats->removeElement($cat);
        }

        return $this;
    }
}
