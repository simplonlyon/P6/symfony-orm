<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180731103143 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE kitten (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, breed VARCHAR(255) NOT NULL, birthdate DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE squat (id INT AUTO_INCREMENT NOT NULL, human_nb INT NOT NULL, location VARCHAR(255) NOT NULL, food TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE squat_kitten (squat_id INT NOT NULL, kitten_id INT NOT NULL, INDEX IDX_42660B088F027BE3 (squat_id), INDEX IDX_42660B086C246F87 (kitten_id), PRIMARY KEY(squat_id, kitten_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE squat_kitten ADD CONSTRAINT FK_42660B088F027BE3 FOREIGN KEY (squat_id) REFERENCES squat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE squat_kitten ADD CONSTRAINT FK_42660B086C246F87 FOREIGN KEY (kitten_id) REFERENCES kitten (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE squat_kitten DROP FOREIGN KEY FK_42660B086C246F87');
        $this->addSql('ALTER TABLE squat_kitten DROP FOREIGN KEY FK_42660B088F027BE3');
        $this->addSql('DROP TABLE kitten');
        $this->addSql('DROP TABLE squat');
        $this->addSql('DROP TABLE squat_kitten');
    }
}
